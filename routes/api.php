<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::namespace('Api')->group(function(){

    /**
     * Cliente
     */
    Route::get('/cliente/getCliente', 'ClienteController@index');
    Route::post('/cliente/crearCliente', 'ClienteController@store');
    Route::get('/cliente/getClienteId/{id}', 'ClienteController@show');
    Route::patch('/cliente/actualizarCliente/{id}', 'ClienteController@update');
    Route::delete('/cliente/eliminarCliente/{id}', 'ClienteController@delete');


    /**
     * Pedidos
     */
    Route::get('/pedido/getPedido', 'PedidoController@index');
    Route::get('/pedido/getPedidoStatus/{estado}', 'PedidoController@index');
    Route::get('/pedido/getPedidoId/{id}/id', 'PedidoController@show');

    Route::post('/pedido/crearPedido', 'PedidoController@store');
    Route::post('/pedido/crearDetallePedido', 'PedidoController@detailOrders');
    Route::post('/pedido/estadoOrden/{id}', 'PedidoController@update');
    
    
    /**
     * Articulo
     */
    Route::get('/articulo/getArticulo', 'ProductController@index');
    Route::get('/articulo/getArticuloId/{id}', 'ProductController@show');
    Route::post('/articulo/crearArticulo', 'ProductController@store');
    Route::patch('/articulo/actualizarArticulo/{id}', 'ProductController@update');
    Route::delete('/articulo/eliminarArticulo/{id}', 'ProductController@delete');
    /**
     * Mostrar detalles de notificación
     */
    // Route::get('/notificaciones/{NotificacionID}', 'NotificacionesController@show')->middleware('is_auth');
});
