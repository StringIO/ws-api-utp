<?php

return [
    /**
     * Base URL
     */
    'base_url' => env('BASE_URL'),

    /**
     * Casilla Electrónica Base URL
     */
    'ce_base_url' => env('CE_BASE_URL'),
];