<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'tbl_cliente';
	protected $primaryKey = 'Id_Cliente';
	public $timestamps = false;
}
