<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    protected $table = 'tbl_articulo';
	protected $primaryKey = 'idArticulo';
	public $timestamps = false;
}
