<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailOrder extends Model
{
    protected $table = 'tbl_detalle_pedido';
	protected $primaryKey = 'Id_Pedido_Detalle';
	public $timestamps = false;
}
