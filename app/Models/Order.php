<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'tbl_pedido';
	protected $primaryKey = 'Id_Pedido';
	public $timestamps = false;
}
