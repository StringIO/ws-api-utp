<?php

namespace App\Packages\Auth;

use Firebase\JWT\JWT;

class JWTPackage {
    /**
     *  Secret Key
     */
    private static $_secretKey = '8VnUj3Sl9k20YJlSDXUikQzQnOVfLn92NHiL0UL9GSjO4ST0Up1ODpQaHChA';

    /**
     * Generate JWT 
     */
    public static function generateJwt(object $data)
    {
        $time = time();

        $token = [
            'iat' => $time,
            'exp' => $time + (7*24*60*60),
            'data' => [
                'UsuarioID' => $data->UsuarioID,
                'PersonaID' => $data->PersonaID,
                'Username' => $data->Username,
                'DatosPersonales' => $data->DatosPersonales,
                'FromApp' => $data->FromApp,
                'NroDocumento' => $data->NroDocumento,
            ]
        ];

        $generate = JWT::encode($token, self::$_secretKey);
        return $generate;
    }

    /**
     * 
     */
    public static function decodeJwt(string $token)
    {
        try {
            return JWT::decode($token, self::$_secretKey, array('HS256'));
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * 
     */
    public static function generateJwtToUser(object $user)
    {   
        $time = time();

        $token = [
            'iat' => $time,
            'exp' => $time + (7*24*60*60),
            'data' => [
                'UsuarioID' => $user->UsuarioID,
                'PersonaID' => $user->PersonaID,
            ]
        ];

        $generate = JWT::encode($token, self::$_secretKey);
        return $generate;
    }

    /**
     * 
     */
}
