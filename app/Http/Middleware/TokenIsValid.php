<?php

namespace App\Http\Middleware;

use App\Packages\Auth\JWTPackage;
use Closure;

class TokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->hasHeader('authorization')) return response()->json(['message' => 'no authenticated'], 401);
        $bearer = $request->bearerToken();
        if(!$bearer) return response()->json(['message' => 'no authenticated'], 401);
        $decodeBearer = JWTPackage::decodeJwt($bearer);
        if(!$decodeBearer) return response()->json(['message' => 'invalid or expired token'], 401);
        return $next($request);
    }
}
