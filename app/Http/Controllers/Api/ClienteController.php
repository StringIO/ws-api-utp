<?php

namespace App\Http\Controllers\Api;

use App\Packages\EmailPackage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\{
    Cliente
};

class ClienteController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        // $this->email = new EmailPackage();   
    }

    public function index()
    {
        try {
            #   Lista de Clientes
            $listCliente = DB::table('tbl_cliente')->get();
            if(!$listCliente) {
                return response()->json(['success' => false, 'message' => 'No se encontró clientes.'], 400);    
            }
            return response()->json(['success' => true, 'data' => [
                'clientes' => $listCliente
            ]], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function show( Request $request, int $id )
    {
        try {
            #   Lista de Clientes
            $listClienteId = DB::table('tbl_cliente')->where('Id_Cliente',$id)->first();
            if(!$listClienteId) {
                return response()->json(['success' => false, 'message' => 'No se encontró cliente.'], 400);    
            }
            return response()->json(['success' => true, 'data' => [
                'clientes' => $listClienteId
            ]], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function store(Request $request)
    {
        try {
            #   Validamos request
            $validator = Validator::make($request->all(), [
                'nombre'        => 'required|string',
                'apellidos'     => 'required|string',
                'telefono'      => 'string',
                'correo'        => 'required|string',
                'nro_documento' => 'required|string',
                'departamento'  => 'string',
                'provincia'     => 'string',
                'distrito'      => 'string',
                'direccion'     => 'required|string',
                'referencia'    => 'string',
            ]);
        
            if($validator->fails()) {
                $errors = $validator->errors();
                throw new \Exception($errors->first());
            }

            #   Obtenemos los datos
            $data = $request->all();
        
            DB::beginTransaction();

            #   Verificamos si usuario existe
            $getDocumento = DB::table('tbl_cliente')
                                ->where('Nro_Documento', $data['nro_documento'])->first();

            #   Si no existe
            if($getDocumento) {
                return response()->json(['success' => false, 'message' => 'Numero de documento ya existe registrado.'], 400);
            }

            $client = new Cliente;
            $client->Nombres        = $data['nombre'];
            $client->Apellidos      = $data['apellidos'];
            $client->Telefono       = $data['telefono'];
            $client->Correo         = $data['correo'];
            $client->Nro_Documento  = $data['nro_documento'];
            $client->Departamento   = $data['departamento'];
            $client->Provincia      = $data['provincia'];
            $client->Distrito       = $data['distrito'];
            $client->Direccion      = $data['direccion'];
            $client->Referencia     = $data['referencia'];
            $client->save();

            // $notificationId = $create[0]->registro;
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Se ha creado el cliente satisfactoriamente'], 201);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function update( Request $request, int $id )
    {
        try {

            $validator = Validator::make($request->all(), [
                'nombre'        => 'required|string',
                'apellidos'     => 'required|string',
                'telefono'      => 'string',
                'correo'        => 'required|string',
                'nro_documento' => 'required|string',
                'departamento'  => 'string',
                'provincia'     => 'string',
                'distrito'      => 'string',
                'direccion'     => 'required|string',
                'referencia'    => 'string',
            ]);
        
            if($validator->fails()) {
                $errors = $validator->errors();
                throw new \Exception($errors->first());
            }

            #   Obtenemos los datos
            $data = $request->all();

            #   Lista de Clientes
            $listClienteId = DB::table('tbl_cliente')->where('Id_Cliente',$id)->first();
            if(!$listClienteId) {
                return response()->json(['success' => false, 'message' => 'No se encontró cliente.'], 400);    
            }

            $client = Cliente::find($id);
            $client->Nombres        = $data['nombre'];
            $client->Apellidos      = $data['apellidos'];
            $client->Telefono       = $data['telefono'];
            $client->Correo         = $data['correo'];
            $client->Nro_Documento  = $data['nro_documento'];
            $client->Departamento   = $data['departamento'];
            $client->Provincia      = $data['provincia'];
            $client->Distrito       = $data['distrito'];
            $client->Direccion      = $data['direccion'];
            $client->Referencia     = $data['referencia'];
            $client->save();

            DB::commit();
            return response()->json(['success' => true, 'message' => 'Se ha actualizó los datos del cliente satisfactoriamente'], 201);

        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function delete(Request $request, int $id)
    {
        try {
            #   Obtenemos Cliente
            $listClienteId = DB::table('tbl_cliente')->where('Id_Cliente',$id)->first();
            if(!$listClienteId) {
                return response()->json(['success' => false, 'message' => 'No se encontró cliente.'], 400);    
            }
            $client = Cliente::find($id);
            $client->delete();
            return response()->json(['success' => true, 'message' => 'Se elimino correctamente el cliente'], 201);

        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}