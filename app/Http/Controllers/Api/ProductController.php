<?php

namespace App\Http\Controllers\Api;

use App\Packages\EmailPackage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\{
    Articulo
};

class ProductController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        // $this->email = new EmailPackage();   
    }

    public function index()
    {
        try {
            #   Lista de Clientes
            $listItems = DB::table('tbl_articulo')->where('Estado',1)->get();
            if(!$listItems) {
                return response()->json(['success' => false, 'message' => 'No se encontró articulo.'], 400);    
            }
            return response()->json(['success' => true, 'data' => [
                'articulos' => $listItems
            ]], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function show( Request $request, string $id )
    {
        try {
            #   Lista de Clientes
            $listItemId = DB::table('tbl_articulo')->where('idArticulo',$id)->first();
            if(!$listItemId) {
                return response()->json(['success' => false, 'message' => 'No se encontró Articulo.'], 400);    
            }
            return response()->json(['success' => true, 'data' => [
                'articulo' => $listItemId
            ]], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function store(Request $request)
    {
        try {
            #   Validamos request
            $validator = Validator::make($request->all(), [
                'descripcion'   => 'required|string',
                'imagen'        => 'required|string',
                'precio'        => 'string',
                'stock'         => 'required|string',
                'talla'         => 'required|string'
            ]);
        
            if($validator->fails()) {
                $errors = $validator->errors();
                throw new \Exception($errors->first());
            }

            #   Obtenemos los datos
            $data = $request->all();
        
            DB::beginTransaction();

            #   Verificamos si usuario existe
            $getItems = DB::table('tbl_articulo')->get();
            $countItems = count($getItems);
            $codZero = str_pad(($countItems +1), 4, "0", STR_PAD_LEFT);
            
            $items = new Articulo;
            $items->idArticulo  = "A".$codZero;
            $items->Descripcion = $data['descripcion'];
            $items->Imagen      = $data['imagen'];
            $items->Precio      = $data['precio'];
            $items->Stock       = $data['stock'];
            $items->Talla       = $data['talla'];
            $items->Estado      = 1;
            $items->save();

            // $notificationId = $create[0]->registro;
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Se ha creado el articulo satisfactoriamente'], 201);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function update( Request $request, string $id )
    {
        try {

            $validator = Validator::make($request->all(), [
                'descripcion'   => 'required|string',
                'imagen'        => 'required|string',
                'precio'        => 'string',
                'stock'         => 'required|string',
                'talla'         => 'required|string'
            ]);
        
            if($validator->fails()) {
                $errors = $validator->errors();
                throw new \Exception($errors->first());
            }

            #   Obtenemos los datos
            $data = $request->all();

            #   Lista de Clientes
            $listClienteId = DB::table('tbl_articulo')->where('idArticulo',$id)->first();
            if(!$listClienteId) {
                return response()->json(['success' => false, 'message' => 'No se encontró articulo.'], 400);    
            }

            $items = Articulo::find($id);
            $items->Descripcion = $data['descripcion'];
            $items->Imagen      = $data['imagen'];
            $items->Precio      = $data['precio'];
            $items->Stock       = $data['stock'];
            $items->Talla       = $data['talla'];
            $items->save();

            DB::commit();
            return response()->json(['success' => true, 'message' => 'Se ha actualizó los datos del articulo satisfactoriamente'], 201);

        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function delete(Request $request, string $id)
    {
        try {
            #   Obtenemos Cliente
            $listClienteId = DB::table('tbl_articulo')->where('idArticulo',$id)->first();
            if(!$listClienteId) {
                return response()->json(['success' => false, 'message' => 'No se encontró articulo.'], 400);    
            }
            $items = Articulo::find($id);
            $items->Estado       = 0;
            $items->save();

            return response()->json(['success' => true, 'message' => 'Se elimino correctamente el articulo'], 201);

        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }
}