<?php

namespace App\Http\Controllers\Api;

use App\Packages\EmailPackage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use XMLWriter;
use Illuminate\Support\Str as Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\{
    Order, DetailOrder
};

class PedidoController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        // $this->email = new EmailPackage();   
    }

    public function index( $estado = 'PF' )
    {
        try {
            #   Lista de Clientes
            $listOrders = DB::table('tbl_pedido')
                            // ->where( 'tbl_pedido.Estado_Pedido', $estado )
                            ->join('tbl_cliente', 'tbl_cliente.Id_Cliente', '=', 'tbl_pedido.Id_Cliente')
                            ->get();
            if( count($listOrders) == 0) {
                return response()->json(['success' => false, 'message' => 'No se encontró pedidos.'], 400);    
            }
            $arrOrders = array();
            foreach ($listOrders as $orders ) {
                $detailOrders = DB::table('tbl_detalle_pedido')->where('Id_Pedido_Detalle',$orders->Id_Pedido)->get();
                $arrOrders[] = array(
                    'cliente'       => $orders->Apellidos.' '.$orders->Nombres,
                    'fecha_pedido'  => $orders->Fecha_Pedido,
                    'estado'        => $orders->Estado_Pedido,
                    'igv'           => $orders->Igv_Pedido,
                    "id"            => $orders->Id_Pedido,
                    "xml"           => is_null($orders->Xml) ? '' : $orders->Xml,
                    'importe'       => $orders->Importe_Final,
                    'detail'        => $detailOrders
                );
            }
            return response()->json(['success' => true, 'data' => $arrOrders ], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function show( Request $request, int $id )
    {
        try {
            #   Lista de Clientes
            $listOrder = DB::table('tbl_pedido')->where( 'tbl_pedido.Id_Pedido', $id )
                            ->join('tbl_cliente', 'tbl_cliente.Id_Cliente', '=', 'tbl_pedido.Id_Cliente')
                            ->first();

            if(!$listOrder) {
                return response()->json(['success' => false, 'message' => 'No se encontró pedidos.'], 400);    
            }

            // $listOrder = DB::table('tbl_pedido')->where( 'Estado_Pedido', $estado )->first();
            $detailOrders = DB::table('tbl_detalle_pedido')->where('Id_Pedido_Detalle',$listOrder->Id_Pedido)->get();

            return response()->json(['success' => true, 'data' => [
                'order' => $listOrder,
                'detail_order' => $detailOrders
            ]], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function store(Request $request)
    {
        try {
            #   Validamos request
            $validator = Validator::make($request->all(), [
                'igv'        => 'required|string',
                'importe'     => 'required|string',
                'cliente'      => 'required'
            ]);
        
            if($validator->fails()) {
                $errors = $validator->errors();
                throw new \Exception($errors->first());
            }

            #   Obtenemos los datos
            $data = $request->all();
        
            DB::beginTransaction();

            #   Verificamos si usuario existe
            $getClient = DB::table('tbl_cliente')
                                ->where('Id_Cliente', $data['cliente'])->first();

            #   Si no existe
            if(!$getClient) {
                return response()->json(['success' => false, 'message' => 'El cliente no existe.'], 400);
            }

            $client = new Order;
            $client->Fecha_Pedido   = date('Y-m-d H:i:s');
            $client->Id_Cliente     = $data['cliente'];
            $client->Estado_Pedido  = "PF";
            $client->Igv_Pedido     = $data['igv'];
            $client->Importe_Final  = $data['importe'];
            $client->save();

            // $notificationId = $create[0]->registro;
            DB::commit();
            return response()->json(['success' => true, 'message' => "Se ha creado el pedido nro: $client->Id_Pedido satisfactoriamente",'codigo' => $client->Id_Pedido], 201);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function detailOrders(Request $request)
    {
        try {
            #   Validamos request
            $validator = Validator::make($request->all(), [
                "articulo"  => 'required|string',
                "descripcion" => 'required|string',
                "talla"     => 'required|string',
                "precio"    => 'required',
                "cantidad"  => 'required',
                "descuento" => 'required',
                "subtotal"  => 'required',
                "idpedido"  => 'required'   
            ]);
        
            if($validator->fails()) {
                $errors = $validator->errors();
                throw new \Exception($errors->first());
            }

            #   Obtenemos los datos
            $data = $request->all();
        
            DB::beginTransaction();

            #   Verificamos si usuario existe
            $getOrder = DB::table('tbl_pedido')
                                ->where('Id_Pedido', $data['idpedido'])->first();

            #   Si no existe
            if(!$getOrder) {
                return response()->json(['success' => false, 'message' => 'El pedido no existe.'], 400);
            }

            $client = new DetailOrder;
            $client->Id_Pedido_Detalle      = $data['idpedido'];
            $client->Id_Articulo            = $data['articulo'];
            $client->Descripcion_Articulo   = $data['descripcion'];
            $client->Talla_Articulo         = $data['talla'];
            $client->Precio_Unitario        = $data['precio'];
            $client->Cantidad               = $data['cantidad'];
            $client->Descuento              = $data['descuento'];
            $client->Sub_importe            = $data['subtotal'];
            $client->save();

            // $notificationId = $create[0]->registro;
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Se ha creado el detalle pedido satisfactoriamente'], 201);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }

    public function update( Request $request, int $id )
    {
        try {

            $validator = Validator::make($request->all(), [
                'estado' => 'required|string',
            ]);
        
            if($validator->fails()) {
                $errors = $validator->errors();
                throw new \Exception($errors->first());
            }

            #   Obtenemos los datos
            $data = $request->all();

            #   Lista de Clientes
            $listClienteId = DB::table('tbl_pedido')->where('Id_Pedido',$id)->first();
            if(!$listClienteId) {
                return response()->json(['success' => false, 'message' => 'No se encontró pedido.'], 400);    
            }

            $client = Order::find($id);
            $client->Estado_Pedido = $data['estado'];
            $client->save();

            $archivo = '';
            if($data['estado'] == 'F')
            {
                $listOrder = DB::table('tbl_pedido')->where( 'Id_Pedido', $id )->first();
                $detailOrders = DB::table('tbl_detalle_pedido')->where('Id_Pedido_Detalle',$listOrder->Id_Pedido)->get();
                $fileXml = $this->EsquemaXml($listOrder, $detailOrders, $id);
                if(!$fileXml){
                    $archivo = "ordersFactura/".$id."/".$id.".xml";
                    $xml = Order::find($id);
                    $xml->Xml = $archivo;
                    $xml->save();
                }
            }

            DB::commit();
            return response()->json([
                'success' => true, 
                'message' => "Se ha actualizó el estado del pedido $listClienteId->Id_Pedido satisfactoriamente",
                'fileOrder' => $archivo
        ], 201);

        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 400);
        }
    }


    public function EsquemaXml($listOrder, $detailOrders,$documento){
        // print_r($data); die();
        $rutaFile = "ordersFactura";
        // dd($data->data);
        header('Content-type: application/xml');
        $objetoXML = new XMLWriter();
        if(file_exists(storage_path("app/public/ordersFactura".$rutaFile."/".$documento))){
            $dir = storage_path("app/public/".$rutaFile."/".$documento);
            $this->rmdir_recursive($dir);
        }

        // Estructura básica del XML
        if(!is_dir(storage_path("app/public/".$rutaFile."/".$documento."/"))){
            mkdir(storage_path("app/public/".$rutaFile."/".$documento));
        }

        $name = $rutaFile."/".$documento."/".$documento.".xml";

        $objetoXML->openURI(storage_path("app/public/".$name));
        $objetoXML->setIndent(true);
        $objetoXML->setIndentString("\t");
        $objetoXML->startDocument('1.0', 'ISO-8859-1');
        // Inicio del nodo raíz
        $objetoXML->startElement("escrituraPublica");
        $objetoXML->writeAttribute("xmlns", "http://com.proveedor.currier/sendCurrier");

            $objetoXML->startElement("envioCurrier");
                $objetoXML->startElement("codigoPedido");
                    $objetoXML->text($documento);
                $objetoXML->endElement();
                $objetoXML->startElement("fechaPedido");
                    $objetoXML->text($listOrder->Fecha_Pedido);
                $objetoXML->endElement();
                $objetoXML->startElement("estadoPedido");
                    $objetoXML->text($listOrder->Estado_Pedido);
                $objetoXML->endElement();
                $objetoXML->startElement("igv");
                    $objetoXML->writeCData($listOrder->Igv_Pedido);
                $objetoXML->endElement();
                $objetoXML->startElement("importeTotal");
                    $objetoXML->text($listOrder->Importe_Final);
                $objetoXML->endElement();

                
                $objetoXML->startElement("presentante");
                    $objetoXML->startElement("codigoPresentante");
                        $objetoXML->text("Representante01");
                    $objetoXML->endElement();
                    $objetoXML->startElement("apellidoPaternoPresentante");
                        $objetoXML->writeCData("Juan Carlos");
                    $objetoXML->endElement();
                    $objetoXML->startElement("apellidoMaternoPresentante");
                        $objetoXML->writeCData("Perez Perez");
                    $objetoXML->endElement();
                    $objetoXML->startElement("nombrePresentante");
                        $objetoXML->writeCData("Juan Carlos");
                    $objetoXML->endElement();
                    $objetoXML->startElement("codigoInstitucion");
                        $objetoXML->text("E0001");
                    $objetoXML->endElement();
                    $objetoXML->startElement("correoElectronicoPresentante");
                        $objetoXML->text("admin@admin.com");
                    $objetoXML->endElement();
                $objetoXML->endElement();

                $objetoXML->startElement("datosPago");
                    $objetoXML->startElement("costoTotalServicio");
                        $objetoXML->text($listOrder->Importe_Final);
                    $objetoXML->endElement();
                    $objetoXML->startElement("codigoFormaPago");
                        $objetoXML->text("TOTAL");
                    $objetoXML->endElement();
                    $objetoXML->startElement("descripcionFormaPago");
                        $objetoXML->text("Pago de Compras");
                    $objetoXML->endElement();
                    $objetoXML->startElement("numeroOperacion");
                        $objetoXML->text("P0000001");
                    $objetoXML->endElement();
                    $objetoXML->startElement("fechaPago");
                        $objetoXML->text(date('d/m/Y'));
                        // $objetoXML->text($listOrder['fechaPago']);
                    $objetoXML->endElement();
                    $objetoXML->startElement("horaPago");
                        // $objetoXML->text($listOrder['horaPago']);
                        $objetoXML->text(date('H:i:s'));
                    $objetoXML->endElement();
                    $objetoXML->startElement("codigoTipoPago");
                        $objetoXML->text("Efectivo");
                    $objetoXML->endElement();
                $objetoXML->endElement();


                $objetoXML->startElement("detalleProductos");
                    foreach ($detailOrders as $detail) {
                        $objetoXML->startElement("item");
                            $objetoXML->text(utf8_encode($detail->Descripcion_Articulo));
                        $objetoXML->endElement();
                        $objetoXML->startElement("cantidad");
                            $objetoXML->text($detail->Cantidad);
                        $objetoXML->endElement();
                        $objetoXML->startElement("talla");
                            $objetoXML->text( $detail->Talla_Articulo );
                        $objetoXML->endElement();   
                    }
                $objetoXML->endElement();

            $objetoXML->endElement();
        $objetoXML->endDocument();

        return false;
        if(file_exists(storage_path("app/public/".$rutaFile."/".$documento))){
            return $name;
        }
    }
    /**
     * @desc Eliminar archivos antes de crearlo
     * @param Request,String - Documento
     * @return Array - Retorna data
    */
    public function rmdir_recursive($dir) {
        $files = scandir($dir);
        array_shift($files);    // remove '.' from array
        array_shift($files);    // remove '..' from array
        foreach ($files as $file) {
            $file = $dir . '/' . $file;
            if (is_dir($file)) {
                $this->rmdir_recursive($file);
                rmdir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dir);
    }

}