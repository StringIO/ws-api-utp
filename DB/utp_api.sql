/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : utp_api

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2021-07-24 15:31:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_articulo`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_articulo`;
CREATE TABLE `tbl_articulo` (
  `idArticulo` varchar(20) NOT NULL,
  `Descripcion` varchar(500) DEFAULT NULL,
  `Imagen` varchar(200) DEFAULT NULL,
  `Precio` decimal(10,2) DEFAULT NULL,
  `Stock` int(11) DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL,
  `Talla` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idArticulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_articulo
-- ----------------------------
INSERT INTO `tbl_articulo` VALUES ('A0001', 'nuevo ar 2', 'dosdvdv.jpg', '1.00', '10', '1', '14');
INSERT INTO `tbl_articulo` VALUES ('A0002', 'nuevo articulo 2', 'dos.jpg', '10.00', '100', '1', '24');

-- ----------------------------
-- Table structure for `tbl_cliente`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_cliente`;
CREATE TABLE `tbl_cliente` (
  `Id_Cliente` int(11) NOT NULL AUTO_INCREMENT,
  `Nombres` varchar(100) DEFAULT NULL,
  `Apellidos` varchar(100) DEFAULT NULL,
  `Telefono` varchar(10) DEFAULT NULL,
  `Correo` varchar(50) DEFAULT NULL,
  `Nro_Documento` varchar(15) DEFAULT NULL,
  `Departamento` varchar(100) DEFAULT NULL,
  `Provincia` varchar(100) DEFAULT NULL,
  `Distrito` varchar(100) DEFAULT NULL,
  `Direccion` varchar(200) DEFAULT NULL,
  `Referencia` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id_Cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_cliente
-- ----------------------------
INSERT INTO `tbl_cliente` VALUES ('1', 'Juan', 'Perez Garcia', '1111111', 'demo@demo.com', '12345678', 'Lima', 'Lima', 'Chosica', 'av lima', 'frente al grifo');
INSERT INTO `tbl_cliente` VALUES ('2', 'miguel2', 'perex', '34567654', 'dem@3eee.ee', '12345678', 'lima', 'lima', 'lima', '--', '...');
INSERT INTO `tbl_cliente` VALUES ('5', 'nuevo', 'dos', '34567654', 'dem33@3eee.ee', '12345668', 'lima', 'lima', 'lima', '--', '...');
INSERT INTO `tbl_cliente` VALUES ('6', 'nuevo UTP', 'dos', '34567654', 'dem33@3eee.ee', '12345662', 'lima', 'lima', 'lima', '--', '...');

-- ----------------------------
-- Table structure for `tbl_detalle_pedido`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_detalle_pedido`;
CREATE TABLE `tbl_detalle_pedido` (
  `Id_Pedido_Detalle` int(11) DEFAULT NULL,
  `Id_Articulo` varchar(5) DEFAULT NULL,
  `Descripcion_Articulo` varchar(100) DEFAULT NULL,
  `Talla_Articulo` int(11) DEFAULT NULL,
  `Precio_Unitario` decimal(8,2) DEFAULT NULL,
  `Cantidad` int(11) DEFAULT NULL,
  `Descuento` decimal(8,2) DEFAULT NULL,
  `Sub_importe` decimal(8,2) DEFAULT NULL,
  KEY `fk_pedido_detalle` (`Id_Pedido_Detalle`) USING BTREE,
  CONSTRAINT `fk_pedido_detalle` FOREIGN KEY (`Id_Pedido_Detalle`) REFERENCES `tbl_pedido` (`Id_Pedido`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_detalle_pedido
-- ----------------------------
INSERT INTO `tbl_detalle_pedido` VALUES ('1', 'A0001', 'Zapatillas Urbanas Ciempies Rosado 25', '25', '78.00', '2', '0.00', '156.00');
INSERT INTO `tbl_detalle_pedido` VALUES ('1', 'A0002', 'Zapatillas', '25', '78.00', '1', '0.00', '100.00');
INSERT INTO `tbl_detalle_pedido` VALUES ('5', 'A0004', 'Articulo 2', '24', '120.00', '2', '0.00', '100.00');
INSERT INTO `tbl_detalle_pedido` VALUES ('6', 'A0040', 'Articulo 40', '24', '120.00', '2', '0.00', '100.00');
INSERT INTO `tbl_detalle_pedido` VALUES ('7', 'A0041', 'Articulo 41', '24', '120.00', '2', '0.00', '100.00');
INSERT INTO `tbl_detalle_pedido` VALUES ('8', 'A0040', 'Articulo 40', '24', '120.00', '2', '0.00', '100.00');
INSERT INTO `tbl_detalle_pedido` VALUES ('9', 'A0001', 'Articulo 40', '24', '120.00', '2', '0.00', '100.00');
INSERT INTO `tbl_detalle_pedido` VALUES ('10', 'A0001', 'Articulo 40', '24', '120.00', '2', '0.00', '100.00');
INSERT INTO `tbl_detalle_pedido` VALUES ('11', 'A0001', 'Articulo 40', '24', '120.00', '2', '0.00', '100.00');

-- ----------------------------
-- Table structure for `tbl_pedido`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_pedido`;
CREATE TABLE `tbl_pedido` (
  `Id_Pedido` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha_Pedido` datetime DEFAULT NULL,
  `Id_Cliente` int(11) DEFAULT NULL,
  `Estado_Pedido` varchar(5) DEFAULT NULL,
  `Igv_Pedido` decimal(8,2) DEFAULT NULL,
  `Importe_Final` decimal(8,2) DEFAULT NULL,
  `Xml` varchar(100) DEFAULT '',
  PRIMARY KEY (`Id_Pedido`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_pedido
-- ----------------------------
INSERT INTO `tbl_pedido` VALUES ('1', '2021-07-06 13:26:20', '1', 'F', '184.08', '156.00', 'ordersFactura/2/2.xml');
INSERT INTO `tbl_pedido` VALUES ('2', '2021-07-17 17:35:03', '1', 'F', '1.18', '200.00', 'ordersFactura/2/2.xml');
INSERT INTO `tbl_pedido` VALUES ('3', '2021-07-17 17:37:22', '17777', 'PF', '1.18', '200.00', null);
INSERT INTO `tbl_pedido` VALUES ('4', '2021-07-17 17:37:50', '17777', 'PF', '1.18', '200.00', null);
INSERT INTO `tbl_pedido` VALUES ('5', '2021-07-17 17:38:23', '1', 'F', '1.18', '200.00', 'ordersFactura/5/5.xml');
INSERT INTO `tbl_pedido` VALUES ('6', '2021-07-17 17:51:13', '2', 'F', '1.18', '500.00', 'ordersFactura/6/6.xml');
INSERT INTO `tbl_pedido` VALUES ('7', '2021-07-17 17:57:26', '2', 'F', '1.18', '600.00', 'ordersFactura/7/7.xml');
INSERT INTO `tbl_pedido` VALUES ('8', '2021-07-24 12:32:21', '2', 'PF', '1.18', '500.00', null);
INSERT INTO `tbl_pedido` VALUES ('9', '2021-07-24 13:22:59', '1', 'PF', '1.18', '500.00', null);
INSERT INTO `tbl_pedido` VALUES ('10', '2021-07-24 15:17:54', '1', 'PF', '1.18', '500.00', '');
INSERT INTO `tbl_pedido` VALUES ('11', '2021-07-24 15:25:37', '1', 'PF', '1.18', '500.00', '');
